package adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import steps.counts.R;

public class StepHistoryAdapter extends RecyclerView.Adapter<StepHistoryAdapter.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<Map> productList;

    //getting the context and product list with constructor
    public StepHistoryAdapter(Context mCtx, List<Map> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public StepHistoryAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.step_item_model, null);
        return new StepHistoryAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StepHistoryAdapter.ProductViewHolder holder, int position) {
        //getting the product of the specified position
        Map product = productList.get(position);
        holder.steps_tv.setText(product.get("steps").toString());
    }


    @Override
    public int getItemCount() {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView steps_tv, status_tv, title_tv, date_tv,position_tv;
        LinearLayout main_ll;

        public ProductViewHolder(View itemView) {
            super(itemView);

            steps_tv = itemView.findViewById(R.id.steps_tv);

        }
    }
}
