package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import steps.counts.InviteEarnActivity;
import steps.counts.R;

public class Friends extends Fragment {
    View view;
    CardView invite_card;
    ImageView gif_image;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.frag_friends,container,false);
        gif_image=view.findViewById(R.id.gif_image);
        invite_card=view.findViewById(R.id.invite_card);
        Glide.with(getActivity()).load("https://opengameart.org/sites/default/files/walk9_0.gif").into(gif_image);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        invite_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), InviteEarnActivity.class));
            }
        });
    }
}
