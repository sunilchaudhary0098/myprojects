package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import steps.counts.EditProfileActivity;
import steps.counts.LevelActivity;
import steps.counts.R;
import steps.counts.StepHistoryActivity;

public class Profile extends Fragment {
    View view;
    CircleImageView c_image_iv;
    TextView history_tv;
    LinearLayout level_ll;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.frag_profile,container,false);
        history_tv=view.findViewById(R.id.history_tv);
        c_image_iv=view.findViewById(R.id.c_image_iv);
        level_ll=view.findViewById(R.id.level_ll);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        history_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), StepHistoryActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition( R.anim.slide_up, R.anim.slide_down );
            }
        });

        c_image_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition( R.anim.slide_up, R.anim.slide_down );
            }
        });

        level_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), LevelActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition( R.anim.slide_up, R.anim.slide_down );
            }
        });
    }
}
