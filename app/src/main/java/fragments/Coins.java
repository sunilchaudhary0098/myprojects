package fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.StepHistoryAdapter;
import steps.counts.R;

public class Coins extends Fragment {
    View view;

    List<Map>mapList;
    RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.frag_coins,container,false);
        setHistory();
        return view;
    }

    private void setHistory(){
        mapList=new ArrayList<>();
        recyclerView=view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Map<String,String>map=new HashMap<>();
        map.put("steps","2561 steps");
        map.put("date","25 Jul");
        map.put("coins","2.05");
        mapList.add(map);
        mapList.add(map);
        mapList.add(map);
        mapList.add(map);

        StepHistoryAdapter adapter=new StepHistoryAdapter(getActivity(),mapList);

        recyclerView.setAdapter(adapter);
    }
}
