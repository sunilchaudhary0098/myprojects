package fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import steps.counts.R;

public class Home extends Fragment implements SensorEventListener {

    SensorManager sensorManager;
    boolean running =false;

    int counter=0;
    TextView steps_tv;
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.frag_home,container,false);
        steps_tv=view.findViewById(R.id.steps_tv);
        sensorManager=(SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
        running=true;
        Sensor countSensor=sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if(countSensor!=null){
            sensorManager.registerListener(this,countSensor,SensorManager.SENSOR_DELAY_UI);
        }else {

        }
    }


    @Override
    public void onPause() {
        super.onPause();
        running=false;
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        if(running){
            Log.v("kdhf",event.values[0]+"");

            steps_tv.setText(""+counter++);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
