package steps.counts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.StepHistoryAdapter;

public class StepHistoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ImageView back_iv;
    List<Map>mapList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_step_history);
        back_iv=findViewById(R.id.back_iv);
        setHistory();
    }

    @Override
    protected void onStart() {
        super.onStart();
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setHistory(){
        mapList=new ArrayList<>();
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        Map<String,String>map=new HashMap<>();
        map.put("steps","2561 steps");
        map.put("date","25 Jul");
        map.put("coins","2.05");
        mapList.add(map);
        mapList.add(map);
        mapList.add(map);
        mapList.add(map);

        StepHistoryAdapter adapter=new StepHistoryAdapter(getApplicationContext(),mapList);

        recyclerView.setAdapter(adapter);
    }
}
