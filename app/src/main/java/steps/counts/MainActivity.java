package steps.counts;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.maps.model.LatLng;

import fragments.Coins;
import fragments.Friends;
import fragments.Home;
import fragments.Profile;

public class MainActivity extends AppCompatActivity {



    public static FragmentManager manager;
    LinearLayout home_ll,coins_ll,friends_ll,profile_ll;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        coins_ll=findViewById(R.id.coins_ll);
        home_ll=findViewById(R.id.home_ll);
        friends_ll=findViewById(R.id.friends_ll);
        profile_ll=findViewById(R.id.profile_ll);
        manager=getSupportFragmentManager();
        FragmentTransaction transaction=manager.beginTransaction();
        transaction.add(R.id.containers,new Home(),null).commit();

    }

    public static  void setFragment(Fragment fragment) {

        FragmentTransaction transaction =
                manager.beginTransaction();
        transaction.replace(R.id.containers, fragment);
        transaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        coins_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new Coins());
            }
        });

        home_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new Home());
            }
        });
        friends_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new Friends());
            }
        });

        profile_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new Profile());
            }
        });
    }
}