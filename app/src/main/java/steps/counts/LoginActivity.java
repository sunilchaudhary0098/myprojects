package steps.counts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;

public class LoginActivity extends AppCompatActivity {

    CardView continue_card;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        getViewID();
    }

    @Override
    protected void onStart() {
        super.onStart();
        continue_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,VerifyActivity.class);
                startActivity(intent);
                overridePendingTransition( R.anim.slide_up, R.anim.slide_down );

            }
        });
    }

    private void getViewID(){
        continue_card=findViewById(R.id.continue_card);
    }
}
